package ru.t1.kravtsov.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
