package ru.t1.kravtsov.tm.command.task;

import ru.t1.kravtsov.tm.api.service.IProjectTaskService;
import ru.t1.kravtsov.tm.api.service.ITaskService;
import ru.t1.kravtsov.tm.command.AbstractCommand;
import ru.t1.kravtsov.tm.enumerated.Role;
import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    protected void displayTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

    protected void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            final String name = task.getName();
            final String description = task.getDescription();
            final String id = task.getId();
            final String status = Status.toName(task.getStatus());
            System.out.printf("%s. (%s) %s : %s : %s\n", index, id, name, description, status);
            index++;
        }
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
