package ru.t1.kravtsov.tm.exception.field;

public final class IdEmptyException extends AbstractFieldException {

    public IdEmptyException() {
        super("Error. Id is empty.");
    }

}
