package ru.t1.kravtsov.tm.command.user;

import ru.t1.kravtsov.tm.enumerated.Role;
import ru.t1.kravtsov.tm.model.User;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractUserCommand {

    public static final String DESCRIPTION = "Registry user.";

    public static final String NAME = "user-registry";

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        final User user = getAuthService().registry(login, password, email);
        displayUser(user);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
