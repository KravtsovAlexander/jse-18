package ru.t1.kravtsov.tm.command.task;

import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "Create new task.";

    public static final String NAME = "task-create";

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        getTaskService().create(getUserId(), name, description);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
