package ru.t1.kravtsov.tm.command.project;

import ru.t1.kravtsov.tm.enumerated.Sort;
import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.model.Project;
import ru.t1.kravtsov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Show project list.";

    public static final String NAME = "project-list";

    @Override
    public void execute() {
        System.out.println("[PROJECTS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> projects = getProjectService().findAll(getUserId(), sort);
        int index = 1;
        for (final Project project : projects) {
            final String name = project.getName();
            final String description = project.getDescription();
            final String id = project.getId();
            final String status = Status.toName(project.getStatus());
            System.out.printf("%s. (%s) %s : %s : %s\n", index, id, name, description, status);
            index++;
        }
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
