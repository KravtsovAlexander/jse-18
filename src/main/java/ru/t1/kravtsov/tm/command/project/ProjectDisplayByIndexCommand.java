package ru.t1.kravtsov.tm.command.project;

import ru.t1.kravtsov.tm.model.Project;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class ProjectDisplayByIndexCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Display project by index.";

    public static final String NAME = "project-display-by-index";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer input = TerminalUtil.nextNumber();
        final Integer index = input - 1;
        final Project project = getProjectService().findOneByIndex(getUserId(), index);
        displayProject(project);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
