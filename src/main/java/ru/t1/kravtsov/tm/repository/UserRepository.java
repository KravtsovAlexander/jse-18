package ru.t1.kravtsov.tm.repository;

import ru.t1.kravtsov.tm.api.repository.IUserRepository;
import ru.t1.kravtsov.tm.model.User;

import java.util.function.Predicate;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    private Predicate<User> filterByLogin(final String login) {
        return m -> login.equals(m.getLogin());
    }

    private Predicate<User> filterByEmail(final String email) {
        return m -> email.equals(m.getEmail());
    }

    public UserRepository() {
    }

    @Override
    public User findByLogin(final String login) {
        return findAll()
                .stream()
                .filter(filterByLogin(login))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return findAll()
                .stream()
                .filter(filterByEmail(email))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Boolean doesLoginExist(final String login) {
        return findAll()
                .stream()
                .anyMatch(filterByLogin(login));
    }

    @Override
    public Boolean doesEmailExist(final String email) {
        return findAll()
                .stream()
                .anyMatch(filterByEmail(email));
    }

}
