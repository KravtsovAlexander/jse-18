package ru.t1.kravtsov.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error. Argument is not supported.");
    }

    public ArgumentNotSupportedException(final String argument) {
        super("Error. Argument \"" + argument + "\" is not supported");
    }

}
