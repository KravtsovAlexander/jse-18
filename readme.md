# TASK MANAGER

## DEVELOPER INFO

**NAME:** Alexander Kravtsov

**E-MAIL:** aekravtsov@nota.tech

## SYSTEM INFO

**OS:** Linux Mint 20.1

**JDK**: Java 1.8

**RAM:** 16GB

**CPU:** i5

## BUILD PROJECT

```
mvn clean install
```

## RUN PROJECT

```
java -jar ./target/task-manager.jar
```