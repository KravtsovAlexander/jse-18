package ru.t1.kravtsov.tm.api.repository;

import ru.t1.kravtsov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    List<Task> removeByName(String userId, String name);

    List<Task> findAllByProjectId(String userId, String projectId);

}
