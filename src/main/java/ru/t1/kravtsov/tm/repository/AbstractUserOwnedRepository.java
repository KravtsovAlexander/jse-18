package ru.t1.kravtsov.tm.repository;

import ru.t1.kravtsov.tm.api.repository.IUserOwnedRepository;
import ru.t1.kravtsov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    protected Predicate<M> filterByUserId(final String userId) {
        return m -> userId.equals(m.getUserId());
    }

    @Override
    public void clear(final String userId) {
        final List<M> models = findAll(userId);
        deleteAll(models);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public List<M> findAll(final String userId) {
        return findAll()
                .stream()
                .filter(filterByUserId(userId))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Override
    public M findOneById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        return findAll()
                .stream()
                .filter(filterByUserId(userId))
                .filter(filterById(id))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final String userId, final Integer index) {
        if (userId == null || index == null) return null;
        return findAll()
                .stream()
                .filter(filterByUserId(userId))
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    @Override
    public int getSize(final String userId) {
        if (userId == null) return 0;
        return (int) findAll()
                .stream()
                .filter(filterByUserId(userId))
                .count();
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        if (userId == null || index == null) return null;
        final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public M add(final String userId, final M model) {
        if (userId == null || model == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Override
    public void deleteAll(final String userId) {
        if (userId == null) return;
        final List<M> models = findAll(userId);
        for (M model : models) {
            remove(userId, model);
        }
    }

}
