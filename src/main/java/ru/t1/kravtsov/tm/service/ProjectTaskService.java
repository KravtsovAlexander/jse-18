package ru.t1.kravtsov.tm.service;

import ru.t1.kravtsov.tm.api.repository.IProjectRepository;
import ru.t1.kravtsov.tm.api.repository.ITaskRepository;
import ru.t1.kravtsov.tm.api.service.IProjectTaskService;
import ru.t1.kravtsov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kravtsov.tm.exception.entity.TaskNotFoundException;
import ru.t1.kravtsov.tm.exception.field.ProjectIdEmptyException;
import ru.t1.kravtsov.tm.exception.field.TaskIdEmptyException;
import ru.t1.kravtsov.tm.exception.field.UserIdEmptyException;
import ru.t1.kravtsov.tm.model.Project;
import ru.t1.kravtsov.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(
            final IProjectRepository projectRepository,
            final ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Task bindTaskToProject(final String userId, final String projectId, final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskFromProject(final String userId, final String projectId, final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeProjectById(final String userId, final String projectId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        if (!projectRepository.existsById(projectId)) return;
        final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        tasks.forEach(t -> taskRepository.removeById(t.getId()));
        projectRepository.removeById(projectId);
    }

    @Override
    public void removeProjects(final String userId, final List<Project> projects) {
        if (userId == null || userId.isEmpty()) return;
        for (final Project project : projects) {
            if (project == null) continue;
            removeProjectById(userId, project.getId());
        }
    }

}
